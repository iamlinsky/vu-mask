import time

millis = lambda: int(round(time.time() * 1000))

class ButtonDebounce():
    def __init__(self, button, timeout, hold=False):
        self.input = button
        self.timeout = timeout
        self.hold = hold
        self.pressed_time = millis()
        self.pressed = False

    def __bool__(self):
        if self.input.value:
            if ((not self.pressed) or self.hold) and millis() - self.pressed_time > self.timeout:
                self.pressed = True
                self.pressed_time = millis()
                return True
            else:
                return False
        else:
            self.pressed = False
            return False
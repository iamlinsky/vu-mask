class ColorMode():
    def __init__(self):
        self.mode = 0
        self.brightness = 0.5

    def update(self):
        self.mode += 1
        if self.mode >= 7:
            self.mode = 0

    # @property
    # def brightness(self):
    #     return self.brightness

    # @brightness.setter
    # def brightness(self, brightness):
    #     self.brightness = brightness

    # G R B
    def get(self, m):
        if m == -1:
            m = int(255*self.brightness)
        else:
            m = int(m*self.brightness)
        if self.mode == 0:
            return (0,m,0)
        elif self.mode == 1:
            return (m,0,0)
        elif self.mode == 2:
            return (0,0,m)
        elif self.mode == 3:
            return (0,m,m)
        elif self.mode == 4:
            return (m,0,m)
        elif self.mode == 5:
            return (0,128,m)
        elif self.mode == 6:
            return (m,m,m)
        else:
            return (0,m,0)
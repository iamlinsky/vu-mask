import array
import math
import time
import digitalio
import audiobusio
import board
from adafruit_circuitplayground.express import cpx

from ledDriver import LedDriver
from colorMode import ColorMode
from buttonDebounce import ButtonDebounce
from ringBuffer import RingBuffer

# Exponential scaling factor.
# Should probably be in range -10 .. 10 to be reasonable.
SCALE_EXPONENT = math.pow(10, 2 * -0.1)
millis = lambda: int(round(time.time() * 1000))

# Restrict value to be between floor and ceiling.
def constrain(value, floor, ceiling):
    return max(floor, min(value, ceiling))

# Scale input_value between output_min and output_max, exponentially.
def log_scale(input_value, input_min, input_max, output_min, output_max):
    normalized_input_value = (input_value - input_min) / (input_max - input_min)
    return output_min +  math.pow(normalized_input_value, SCALE_EXPONENT) * (output_max - output_min)

# Remove DC bias before computing RMS.
def normalized_rms(values):
    minbuf = int(mean(values))
    samples_sum = sum(
        float(sample - minbuf) * (sample - minbuf)
        for sample in values
    )
    return math.sqrt(samples_sum / len(values))

def mean(values):
    return sum(values) / len(values)


# Set up NeoPixels and turn them all off.
cpx.pixels.deinit()
pixels = LedDriver(board.NEOPIXEL, 10)
horns = LedDriver(board.A7, 32)

# Microphone setup
mic = audiobusio.PDMIn(board.MICROPHONE_CLOCK, board.MICROPHONE_DATA, sample_rate=16000, bit_depth=16)
samples = array.array('H', [0] * 150)

# Upper and lower bounds
input_floor = 50
input_ceiling = input_floor + 250

# Upper and lower time constants
alpha_upper = (1/50.0)
alpha_lower = (1/10.0)

# Upper and lower peaks
max_vu = 0
min_vu = 100000

# Ring buffer for moving average
ring_buffer = RingBuffer(50)
color = ColorMode()

button_a = ButtonDebounce(cpx._a, 50)

# Call cpx.touch_Ai to initialize TouchIn object before accessing cpx._touches[i] directly
touch_1 = cpx.touch_A4
touch_1 = ButtonDebounce(cpx._touches[4], 50)
# print(cpx._touches[1].threshold) # Default set to 1010

touch_4 = cpx.touch_A5
touch_4 = ButtonDebounce(cpx._touches[5], 50)

touch_2 = cpx.touch_A2
touch_2 = ButtonDebounce(cpx._touches[2], 1, hold=True)

touch_3 = cpx.touch_A3
touch_3 = ButtonDebounce(cpx._touches[3], 1, hold=True)

switch = 0


while True:
    # Collect data
    mic.record(samples, len(samples))
    magnitude = normalized_rms(samples)

    # Update upper and lower bounds
    ring_buffer.push(magnitude)
    max_vu = ring_buffer.average + (ring_buffer.average * 0.5)
    min_vu = ring_buffer.average - (ring_buffer.average * 0.2)

    if magnitude > max_vu:
        input_ceiling += alpha_upper * (magnitude - input_ceiling)

    if magnitude < min_vu:
        input_floor += alpha_lower * (magnitude - input_floor)

    # Compute scaled logarithmic reading in the range 0 to NUM_PIXELS
    c = int(log_scale(constrain(magnitude, input_floor, input_ceiling), input_floor, input_ceiling, 0, 255))
    
    # Set LED colors
    if switch == 0:
        pixels.all_on(color.get(c))
        horns.all_on(color.get(c))
    elif switch == 1:
        # rainbow face
        pixels.commet(color.brightness, 10)
        horns.all_on((0,0,0))   
        pass
    elif switch == 2:
        # rainbow horns
        pixels.all_on((0,0,0))
        horns.commet(color.brightness, 8)
    elif switch == 3:
        # pulse horns
        pixels.all_on((0,0,0))
        horns.pulse(color.get(-1))
    elif switch == 4:
        # pulse face
        pixels.pulse(color.get(-1))
        horns.all_on((0,0,0))        
    else:
        pass


    if touch_1:
        color.update()

    if touch_2:
        color.brightness = constrain(color.brightness + 0.1, 0.0, 1.0)

    if touch_3:
        color.brightness = constrain(color.brightness - 0.1, 0.0, 1.0)

    if touch_4:
        switch += 1
        if switch == 5:
            switch = 0
    
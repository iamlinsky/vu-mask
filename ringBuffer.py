class RingBuffer():
    def __init__(self, length):
        self.length = length
        self.samples = [0]*length
        self.index = 0
        self.average = 0

    def push(self, value):
        self.samples[self.index] = value
        self.index += 1
        if self.index >= self.length:
            self.index = 0
        self.average = sum(self.samples) / len(self.samples)
import math
import neopixel_write
import digitalio

class LedDriver():
    def __init__(self, pin, count):
        self.count = count
        self.pixel_array = bytearray(self.count*3)
        self.pixel_pin = digitalio.DigitalInOut(pin)
        self.pixel_pin.direction = digitalio.Direction.OUTPUT
        self.set_all((0,0,0))
        self.mode = 0
        self.wheel_offset = 0
        self.wheel_index = 0
        # Pulse data
        self.sin_array = []
        self.sin_index = 0
        for i in range(0,128):
            self.sin_array.append( round( math.sin(math.pi*(i/127)),3 ) )
        # Commet
        self.commet_index = 0
        self.pixel_index = 0

    def set_all(self, color):
        for i in range(0, self.count*3):
            self.pixel_array[i] = color[i%3]

    def set_led(self, index, color):
        i = index*3
        self.pixel_array[i : i+3] = bytes(color)

    def write_leds(self):
        neopixel_write.neopixel_write(self.pixel_pin, self.pixel_array)

    def all_on(self, color):
        self.set_all(color)
        self.write_leds()

    def pulse(self, color):
        c = (int(color[0]*self.sin_array[self.sin_index]), int(color[1]*self.sin_array[self.sin_index]), int(color[2]*self.sin_array[self.sin_index]))
        self.set_all(c)
        self.write_leds()
        self.sin_index += 1
        if self.sin_index == len(self.sin_array):
            self.sin_index = 0

    def color_wheel(self, pos):
        # Input a value 0 to 255 to get a color value.
        # The colours are a transition r - g - b - back to r.
        if (pos < 0) or (pos > 255):
            return (0, 0, 0)
        if pos < 85:
            return (int(255 - pos*3), int(pos*3), 0)
        elif pos < 170:
            pos -= 85
            return (0, int(255 - (pos*3)), int(pos*3))
        else:
            pos -= 170
        return (int(pos*3), 0, int(255 - pos*3))

    def commet(self, brightness, mod):
        for p in range(self.count):
            color = self.color_wheel(25 * ((self.pixel_index + p)%mod))
            c = tuple([int((c*brightness) * ( (10 - (self.pixel_index+p)%10)) / 10.0) for c in color])
            self.set_led(p, c)
        self.write_leds()
        self.pixel_index += 1
        if self.pixel_index == self.count:
            self.pixel_index = 0